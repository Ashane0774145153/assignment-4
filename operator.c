#include <stdio.h>

int main() {

    int a = 10;	/*Value of a is 10 */  
    int b = 15;	/*Value of b is 15*/
   int c = 0;  /*C is used to assign the result of each operator*/   
printf("Please note that A carries the value of 10 and B carries 15 in the course of this demonstration of selected bitwise operators \n");      

   printf("This example will show the result of A AND B (A&B)");
   c = a & b;       
   printf("=Example 1 - Value of c is %d\n", c );
   printf("This example will show the result of A XOR B (A^B)");
   c = a ^ b;       
   printf("=Example 3 - Value of c is %d\n", c );
   printf("This example will show the result of One's compliment on A (~A)");
   c = ~a;          
   printf("=Example 4 - Value of c is %d\n", c );
   printf("This example will show the result of Binary left shift on A (A<<3)");
   c = a << 3;     
   printf("=Example 5 - Value of c is %d\n", c );
   printf("This example will show the result of Binary right shift on B (B>>3)");
   c = b >> 3;     
   printf("=Example 6 - Value of c is %d\n", c );
}